%%%-------------------------------------------------------------------
%% @doc erlapp public API
%% @end
%%%-------------------------------------------------------------------

-module(erlapp_app).

-behaviour(application).

%% Application callbacks

-export([start/2, stop/1]).

%%====================================================================
%% API
%%====================================================================

% start(_StartType, _StartArgs) ->
%     erlapp_sup:start_link().

start(_StartType, _StartArgs) ->
    {ok, Pid} = erlapp_sup:start_link(),
    Routes = [{'_', [{"/", erlapp_root, []}]}],
    Dispatch = cowboy_router:compile(Routes),
    TransOpts = [{ip, {0, 0, 0, 0}}, {port, 2938}],
    ProtoOpts = #{env => #{dispatch => Dispatch}},
    {middlewares,
     [cowboy_router, erlapp_log, cowboy_handler]},
    {ok, _} = cowboy:start_clear(handler, TransOpts,
				 ProtoOpts),
    {ok, Pid}.

%%--------------------------------------------------------------------
stop(_State) -> ok.

%%====================================================================
%% Internal functions
%%====================================================================

