-module(erlapp_log).

-behaviour(cowboy_middleware).

-export([execute/2]).

execute(Req, Env) ->
    {{Peer, _}, Req2} = cowboy_req:peer(Req),
    {Method, Req3} = cowboy_req:method(Req2),
    {Path, Req4} = cowboy_req:path(Req3),
    error_logger:info_msg("~p: [~p]: ~p ~p",
			  [calendar:universal_time(), Peer, Method, Path]),
    {ok, Req4, Env}.
